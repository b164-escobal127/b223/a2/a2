<?php require_once("./code.php")?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Divisible of Five</h2>
    <?php divisibleByFive();?>

    <h3>Array Manipulation</h3>

    <h4>Students Name</h4>
    <?php array_push($students, 'James')?>
    <pre><?php print_r($students) ?></pre>
    <h4>Students count</h4>
    <pre><?php echo count($students); ?></pre>

    <h4>Student update after adding</h4>
    <?php array_push($students, 'Kevin')?>
    <pre><?php print_r($students) ?></pre>
    <h4>Students count after adding</h4>
    <pre><?php echo count($students); ?></pre>

    <h4>Students name after removing</h4>
    <?php array_shift($students)?>
    <pre><?php print_r($students) ?></pre>
    <h4>Students count after removing</h4>
    <pre><?php echo count($students); ?></pre>

</body>
</html>
